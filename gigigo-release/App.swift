//
//  App.swift
//  gigigo-release
//
//  Created by José Estela on 28/2/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

struct AppArgs {
    
    let projectPath: String
    var scheme: String?
    // let build: Bool
    // let test: Bool
    
}

struct App {
    
    /// Command line program secuence
    ///
    /// - Parameter args: args of app
    static func run(args: [String]? = nil) {
        guard let args = args else { exit(1) }
        let argsValidation = Arguments.validateArgs(args: args)
        if argsValidation.validated {
            start(args: args)
        } else {
            if let error = argsValidation.error {
                LogError(error)
            }
            exit(1)
        }
    }
    
    private static func start(args: [String]) {
        let appArgs = self.appArgs(for: args)
        var current = appArgs.projectPath
        if System.exist(path: current), System.find(in: current, contains: ".xcodeproj") {
            // Remove the last '/' if exist
            if let last = current.indices.last, current.last == "/" {
                current.remove(at: last)
            }
            Log("Creating release from \(current)")
            let releasePath = current + "-release/"
            Log("Copying to \(releasePath) directory")
            System.copyItem(at: current, to: releasePath)
            Log("Removing git")
            System.removeItem(at: releasePath + ".git")
            Log("Removing gitignore")
            System.removeItem(at: releasePath + ".gitignore")
            Log("Removing fastlane")
            System.removeItem(at: releasePath + "fastlane")
            Log("Removing Carthage checkouts")
            System.removeItem(at: releasePath + "Carthage/Checkouts")
            _ = self.keepScheme(in: releasePath, with: appArgs)
            /*if let scheme = scheme, appArgs.build {
                guard let releaseItems = System.listItems(in: releasePath) else { return }
                var project: String?
                for item in releaseItems where item.contains(".xcodeproj") {
                    project = item
                }
                guard
                    let projectName = project
                else {
                    return
                }
                self.buildProject(releasePath + projectName, with: scheme, testing: appArgs.test)
            }*/
        } else {
            LogError("The project path is not correct (maybe it doesn't contain a .xcodeproj file): \(current)")
            exit(1)
        }
    }
    
    /*private static func buildProject(_ projectPath: String, with scheme: String, testing: Bool) {
        let result = System.executeWithResult(
            command: "/usr/bin/xcodebuild",
            with: [
                "-scheme",
                scheme,
                "-project",
                projectPath,
                "-configuration",
                "Release",
                "-destination",
                "platform=iOS Simulator,name=iPhone 6 Plus,OS=11.1"
            ]
        )
        print(result)
    }*/
    
    private static func keepScheme(in releasePath: String, with appArgs: AppArgs) -> String? {
        guard let releaseItems = System.listItems(in: releasePath) else { return nil }
        var project: String?
        for item in releaseItems where item.contains(".xcodeproj") {
            project = item
        }
        guard
            let projectName = project,
            let items = System.listItems(in: releasePath + projectName + "/xcshareddata/xcschemes/")
        else {
            LogError("This release doesn't exist. Please, re-try and select a correct scheme.")
            return nil
        }
        let schemesFolder = releasePath + projectName + "/xcshareddata/xcschemes/"
        var schemeItem = -1
        if let scheme = appArgs.scheme {
            schemeItem = items.index(of: scheme + ".xcscheme") ?? -1
        } else {
            Log("Getting schemes... ")
            for (index, item) in items.enumerated() {
                Log("\(index) => \(item)")
            }
            Log("Select release scheme (the others will be deleted):")
            let read = readLine()
            guard let selected = read, let selectedItem = Int(selected) else {
                LogError("Insert a number between 0 and \(items.count - 1)")
                return nil
            }
            schemeItem = selectedItem
        }
        if items.indices.contains(schemeItem) {
            LogInfo("\(items[schemeItem]) keeped :)")
            for (index, item) in items.enumerated() where index != schemeItem {
                System.removeItem(at: schemesFolder + item)
            }
            return items[schemeItem].replacingOccurrences(of: ".xcscheme", with: "")
        } else {
            guard let scheme = appArgs.scheme else {
                LogError("Insert a number between 0 and \(items.count - 1)")
                return nil
            }
            LogError("The given scheme is not correct \(scheme)")
        }
        return nil
    }
    
    private static func appArgs(for args: [String]) -> AppArgs {
        let projectPath = args[1]
        let scheme = args.filter({ $0 != "--build" && $0 != "--test" && $0 != projectPath && $0 != args[0] }).first
        // let build = args.filter({ $0 == "--build" }).count > 0
        // let test = args.filter({ $0 == "--test" }).count > 0
        return AppArgs(projectPath: projectPath, scheme: scheme)
    }
}
