//
//  Arguments.swift
//  gigigo-release
//
//  Created by José Estela on 2/3/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

struct Arguments {
    
    /// Call this method to validate program arguments
    ///
    /// - Parameter args: args of program
    /// - Returns: if validated and error
    static func validateArgs(args: [String]) -> (validated: Bool, error: String?) {
        return rules(for: args) ? (validated: true, error: nil) : (validated: false, error: help())
    }
    
    /// Show help of program
    ///
    /// - Returns: The help
    static func help() -> String {
        return """
        Gigigo release usage \n\n
        >> gigigo-release path/to/project [scheme-name] [--build] [--test] \n
        Where:\n
        - path/to/project -> the complete path to the folder that contains the project \n
        - scheme-name (optional) -> the scheme to be keeped (if it is not setted, the program will ask you) \n
        - build (optional) -> if you want to build the project before generating the release. \n
        - test (optional) -> if you want to pass all tests before generating the release. \n
        """
    }
}

private extension Arguments {
    
    /// Add custom rules to check arguments
    ///
    /// - Parameter args: args of program
    /// - Returns: if validated
    static func rules(for args: [String]) -> Bool {
        if args.count >= 2 && args.count < 6 {
            return true
        }
        return false
    }
}
