//
//  main.swift
//  gigigo-release
//
//  Created by José Estela on 28/2/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

// Perform the application 
App.run(args: CommandLine.arguments)
