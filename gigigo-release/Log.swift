//
//  Log.swift
//  gigigo-templates
//
//  Created by José Estela on 2/3/17.
//  Copyright © 2017 José Estela. All rights reserved.
//

import Foundation

public enum LogLevel: Int {
    /// No log will be shown.
    case none = 0
    
    /// Only warnings and errors.
    case error = 1
    
    /// Errors and relevant information.
    case info = 2
    
    /// Request and Responses will be displayed.
    case debug = 3
}

public enum LogColor: String {
    case black = "\u{001B}[0;30m"
    case red = "\u{001B}[0;31m"
    case green = "\u{001B}[0;32m"
    case yellow = "\u{001B}[0;33m"
    case blue = "\u{001B}[0;34m"
    case magenta = "\u{001B}[0;35m"
    case cyan = "\u{001B}[0;36m"
    case white = "\u{001B}[0;37m"
}

public func >= (levelA: LogLevel, levelB: LogLevel) -> Bool {
    return levelA.rawValue >= levelB.rawValue
}


public class LogManager {
    public static let shared = LogManager()
    
    public var appName: String?
    public var logLevel: LogLevel = .debug
}


public func Log(_ log: String, color: LogColor = .white) {
    guard LogManager.shared.logLevel != .none else { return }
    print(color.rawValue + log + "\u{001B}[0;0m")
}

public func LogInfo(_ log: String) {
    guard LogManager.shared.logLevel >= .info else { return }
    
    Log(log, color: .green)
}

public func LogLine() {
    print()
}

public func LogWarn(_ message: String, filename: NSString = #file, line: Int = #line, funcname: String = #function) {
    guard LogManager.shared.logLevel >= .error else { return }
    Log(message, color: .yellow)
}

func LogError(_ error: String, filename: NSString = #file, line: Int = #line, funcname: String = #function) {
    guard
        LogManager.shared.logLevel >= .error
        else { return }
    Log(error, color: .red)
}
